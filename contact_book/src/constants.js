import $ from "jquery";

export const CONTACT_LIST = 'contactList';
export const URL = 'http://demo.sibers.com/users';
export const DEFAULT_CONTACT = {
    "name": "",
    "username": "",
    "email": "",
    "address": {
        "streetA": "",
        "streetB": "",
        "streetC": "",
        "streetD": "",
        "city": "",
        "state": "",
        "country": "",
        "zipcode": "",
        "geo": {
            "lat": "",
            "lng": ""
        }
    },
    "phone": "",
    "website": "",
    "company": {
        "name": "",
        "catchPhrase": "",
        "bs": ""
    },
    "posts": [],
    "accountHistory": [],
    "favorite": false,
    "avatar": "https://via.placeholder.com/150",
    "id": null
};
export const CONTACT_INFO_DEFAULT_STATE = {
    info: DEFAULT_CONTACT,
    avatar: '',
    name: '',
    username: '',
    email: '',
    phone: '',
    website: '',
    address: {
        streetA: "",
        streetB: "",
        streetC: "",
        streetD: "",
        city: "",
        state: "",
        country: "",
        zipcode: "",
        geo: {
            lat: "",
            lng: ""
        }
    },
    company: {
        name: "",
        catchPhrase: "",
        bs: ""
    },

};

export function getFromLocalStorage(contactId) {
    return JSON.parse(localStorage.getItem(CONTACT_LIST))[contactId];
}

export function setContactToLocalStorage(contact) {
    let data = JSON.parse(localStorage.getItem(CONTACT_LIST));
    data[contact.id] = contact;
    localStorage.setItem(CONTACT_LIST, data);
}

export function getRequest(url) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'json',
            success: resolve,
            error: reject
        });
    });
}
