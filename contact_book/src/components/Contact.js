import React from "react";
import Media from "react-bootstrap/Media";
import ListGroupItem from "react-bootstrap/ListGroupItem";

class Contact extends React.Component {

    render() {
        return (
            <ListGroupItem
                action
                onClick={() => {this.props.onClick(this.props.id)}}
                className={this.props.bgColor + ' ' + this.props.textColor + " pl-3 pl-3"}
            >
                <Media>
                    <img
                        width={64}
                        height={64}
                        className="mr-3 rounded"
                        src={this.props.avatar}
                        alt="Generic placeholder"
                    />
                    <Media.Body>
                        <h5>{this.props.name}</h5>
                        <p>
                            {this.props.phone}
                        </p>
                    </Media.Body>
                </Media>
            </ListGroupItem>
        )
    }
}

export default Contact;