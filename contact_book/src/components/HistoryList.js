import React from 'react';
import '../index.css';
import Table from "react-bootstrap/Table";

class HistoryList extends React.Component {

    render() {
        return (
            <div className="m-3">
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Amount</th>
                        <th>Date</th>
                        <th>Business</th>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Account</th>
                    </tr>
                </thead>
                <tbody className="vh-100 overflow-auto">
                {
                    this.props.historyList ? this.props.historyList.map(
                        (elem, index) => {
                            let date = new Date(elem.date).toString();
                            return (
                                <tr key={index}>
                                    <th>{index + 1}</th>
                                    <th>{elem.amount}</th>
                                    <th>{date}</th>
                                    <th>{elem.business}</th>
                                    <th>{elem.name}</th>
                                    <th>{elem.type}</th>
                                    <th>{elem.account}</th>
                                </tr>
                            )
                        }
                    ) : <tr/>
                }
                </tbody>
            </Table>
            </div>
        )
    }
}

export default HistoryList;