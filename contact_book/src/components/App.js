import React from 'react';
import ContactList from './ContactList'
import TabPanel from "./TabPanel";

// Bootstrap

import 'bootstrap/dist/css/bootstrap.css';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";
import {DEFAULT_CONTACT} from "../constants";
import Form from "react-bootstrap/Form";

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            contactList: this.props.contactList,
            activeContactId: null,
            search: ''
        };
        this.updateActiveContact = this.updateActiveContact.bind(this);
        this.updateOneContact = this.updateOneContact.bind(this);
        this.handleChangeSearch = this.handleChangeSearch.bind(this);
    }

    updateOneContact(contact) {
        let contactList = this.state.contactList;
        contactList[contact.id] = contact;
        this.setState((state) => ({
            contactList: contactList,
            activeContactId: state.activeContactId,
            search: state.search
        }));
    }

    updateActiveContact(contactId) {
        this.setState((state) => ({
            contactList: state.contactList,
            activeContactId: contactId,
            search: state.search
        }));
    }

    handleChangeSearch(event) {
        let newSearchValue = event.target.value;
        this.setState((state) => ({
            contactList: state.contactList,
            activeContactId: state.activeContactId,
            search: newSearchValue
        }))
    }

    render() {
        const activeContact = this.state.activeContactId !== null ?
            this.state.contactList[this.state.activeContactId] :
            DEFAULT_CONTACT;
        const activeContactId = activeContact.id;
        return (
            <Container className="bg-light">
                <Row>
                    <Col md={4} className="border">
                        <Form className='mt-3'>
                            <Form.Group controlId="search">
                                <Form.Control
                                    placeholder="Search..."
                                    aria-label="search"
                                    onChange={(e) => this.handleChangeSearch(e)}
                                    value={this.state.search}
                                />
                            </Form.Group>
                        </Form>
                        <hr/>
                        <ContactList
                            updateActiveContact={this.updateActiveContact}
                            contactList={this.state.contactList}
                            activeContactId={activeContactId}
                            searchText={this.state.search}
                        />
                    </Col>
                    <Col md={8} className="bg-white border">
                        <Row>
                            <TabPanel
                                updateState={this.updateOneContact}
                                contact={activeContact}
                            />
                        </Row>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default App;