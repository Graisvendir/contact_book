import React from 'react';
import '../index.css';
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import Col from "react-bootstrap/Col";
import HistoryList from "./HistoryList";
import PostList from './PostList'
import ContactInfo from "./ContactInfo";

class TabPanel extends React.Component {

    render() {
        let posts = null, history = null;
        if (this.props.contact) {
            posts = this.props.contact.posts;
            history = this.props.contact.accountHistory;
        }
        return (
            <Col className="pl-0 pr-0">
                <Tabs className="bg-light justify-content-center pt-3 mb-3" defaultActiveKey="call-list" id="contact-tabs">
                    <Tab eventKey="call-list" title="Account History">
                        <HistoryList historyList={history ? history : null}/>
                    </Tab>
                    <Tab eventKey="message-list" title="Posts">
                        <PostList postList={posts ? posts : null}/>
                    </Tab>
                    <Tab eventKey="info" title="Contact Info">
                        <ContactInfo
                            updateState={this.props.updateState}
                            info={this.props.contact ? this.props.contact : null}
                        />
                    </Tab>
                </Tabs>
            </Col>
        );
    }
}

export default TabPanel;