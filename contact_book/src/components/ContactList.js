import React from 'react';
import '../index.css';
import Contact from './Contact'
import ListGroup from "react-bootstrap/ListGroup";

class ContactList extends React.Component {

    render() {
        return (
            <ListGroup className="vh-100 overflow-auto">
                {this.props.contactList.map(
                    (contact) => {
                        // console.log(contact.id);
                        let bgColor = contact.id === this.props.activeContactId ? 'bg-primary' : '';
                        let textColor = contact.id === this.props.activeContactId ? 'text-white' : '';
                        let result = contact.name.toLowerCase().search(this.props.searchText);
                        if (result !== -1) {
                            return (
                                <Contact
                                    onClick={this.props.updateActiveContact}
                                    key={contact.id}
                                    name={contact.name}
                                    phone={contact.phone}
                                    avatar={contact.avatar}
                                    id={contact.id}
                                    bgColor={bgColor}
                                    textColor={textColor}
                                />
                            )
                        }

                    }
                )}
            </ListGroup>
        )
    }
}

export default ContactList;