import React from 'react';
import '../index.css';
import ListGroup from "react-bootstrap/ListGroup";
import Media from "react-bootstrap/Media";

class PostList extends React.Component {
    render() {
        return (
            <ListGroup className="vh-100 overflow-auto" variant="flush">
                { this.props.postList ? this.props.postList.map(
                    (elem, index) => {
                        return (
                        <ListGroup.Item key={index}>
                            <Media>
                                <Media.Body>
                                    <h3>{elem.words.toString()}</h3>
                                    <h4>{elem.sentence}</h4>
                                    <p>{elem.sentences}</p>
                                    <p>{elem.paragraph}</p>
                                </Media.Body>
                            </Media>
                        </ListGroup.Item>
                        );
                    }
                ) : <div/> }

            </ListGroup>

        )
    }
}

export default PostList;