import React from 'react';
import '../index.css';
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import InputGroup from "react-bootstrap/InputGroup";
import Image from "react-bootstrap/Image";
import Button from "react-bootstrap/Button";
import personIcon from '../icons/person.svg'
import locationIcon from '../icons/location.svg';
import phoneIcon from '../icons/phone.svg';
import {CONTACT_INFO_DEFAULT_STATE, setContactToLocalStorage} from '../constants'

class ContactInfo extends React.Component {

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = CONTACT_INFO_DEFAULT_STATE;
    }

    handleSubmit(event) {
        let contact = this.props.info;
        contact[event.target.id] = event.target.value;
        setContactToLocalStorage(contact);
    }

    handleChange(event) {
        let contact = this.props.info;
        contact[event.target.id] = event.target.value;
        this.props.updateState(contact);
    }

    render() {
        let avatar = '';
        let name = '';
        let username = '';
        let email = '';
        let phone = '';
        let website = '';
        let address = {};
        let company = {};
        if (this.props.info) {
            avatar = this.props.info.avatar;
            name = this.props.info.name;
            username = this.props.info.username;
            email = this.props.info.email;
            phone = this.props.info.phone;
            website = this.props.info.website;
            address = this.props.info.address;
            company = this.props.info.company;
        }
        return (
            <Form>
                <div className='w-100 border-bottom'>
                    <Button
                        onClick={this.handleSubmit}
                        className='mb-2 btn-lg mx-auto d-block'
                        type="button"
                    >
                        Save
                    </Button>
                </div>
                <div className='vh-100 overflow-auto p-3'>
                    <Form.Group controlId="avatar">
                        <Image rounded className='mx-auto d-block' src={avatar} alt="person" />
                        <Form.Label>Avatar URL</Form.Label>
                        <Form.Control onChange={this.handleChange} type="url" placeholder="www.google.com" value={avatar} />
                        <Form.Control.Feedback type="invalid">
                            Please enter valid url.
                        </Form.Control.Feedback>
                    </Form.Group>

                    <hr/>
                    <Image className='icon mx-auto d-block' src={personIcon} alt="person" />


                    <Form.Group controlId="name">
                        <Form.Label>Name</Form.Label>
                        <Form.Control
                            onChange={this.handleChange}
                            type="text"
                            placeholder="John Connor"
                            value={name}
                        />
                    </Form.Group>
                    <Form.Group controlId="username">
                        <Form.Label>Username</Form.Label>
                        <Form.Control
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Like.God"
                            value={username}
                        />
                    </Form.Group>
                    <Form.Group controlId="email">
                        <Form.Label>Email</Form.Label>
                        <InputGroup>
                            <InputGroup.Prepend>
                                <InputGroup.Text id="inputGroupPrepend">@</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                onChange={this.handleChange}
                                type="email"
                                placeholder="WhatIfGodWasOneOfUs@gmail.com"
                                aria-describedby="inputGroupPrepend"
                                value={email}
                                required
                            />
                            <Form.Control.Feedback type="invalid">
                                Please choose valid email.
                            </Form.Control.Feedback>
                        </InputGroup>
                    </Form.Group>
                    <hr/>
                    <Image className='icon mx-auto d-block' src={phoneIcon} alt="person" />
                    <Form.Group controlId="phone">
                        <Form.Label>Phone Number</Form.Label>
                        <Form.Control
                            onChange={this.handleChange}
                            type="text"
                            placeholder="534.543.13462"
                            value={phone}
                        />
                    </Form.Group>
                    <Form.Group controlId="website">
                        <Form.Label>Website</Form.Label>
                        <Form.Control
                            onChange={this.handleChange}
                            type="text"
                            placeholder="www.your-website.com"
                            value={website}
                        />
                    </Form.Group>
                    <hr/>
                    <Image className='icon mx-auto d-block' src={locationIcon} alt="person" />
                    <Form.Row>
                        <Form.Group as={Col} controlId="address.geo.lat">
                            <Form.Label>Latitude</Form.Label>
                            <Form.Control
                                onChange={this.handleChange}
                                type="text"
                                placeholder="50"
                                value={address.geo.lat}
                            />
                        </Form.Group>
                        <Form.Group as={Col} controlId="address.geo.lng">
                            <Form.Label>Longitude</Form.Label>
                            <Form.Control

                                onChange={this.handleChange}
                                type="text"
                                placeholder="60"
                                value={address.geo.lng}
                            />
                        </Form.Group>
                    </Form.Row>
                    <Form.Row>
                        <Form.Group as={Col} controlId="country">
                            <Form.Label>Country</Form.Label>
                            <Form.Control
                                onChange={this.handleChange}
                                type="text"
                                placeholder="Russia"
                                value={address.country}
                            />
                        </Form.Group>
                        <Form.Group as={Col} controlId="state">
                            <Form.Label>State</Form.Label>
                            <Form.Control
                                onChange={this.handleChange}
                                type="text"
                                placeholder="Altai cry"
                                value={address.country}
                            />
                        </Form.Group>
                    </Form.Row>
                    <Form.Row>
                        <Form.Group as={Col} controlId="city">
                            <Form.Label>City</Form.Label>
                            <Form.Control
                                onChange={this.handleChange}
                                type="text"
                                placeholder="Barnaul"
                                value={address.city}
                            />
                        </Form.Group>
                        <Form.Group as={Col} controlId="zipcode">
                            <Form.Label>Zipcode</Form.Label>
                            <Form.Control
                                onChange={this.handleChange}
                                type="text"
                                placeholder="656000"
                                value={address.zipcode}
                            />
                        </Form.Group>
                    </Form.Row>
                    <Form.Group controlId="streetA">
                        <Form.Label>Street A</Form.Label>
                        <Form.Control
                            onChange={this.handleChange}
                            type="text"
                            placeholder=""
                            value={address.streetA}
                        />
                    </Form.Group>
                    <Form.Group controlId="streetB">
                        <Form.Label>Street B</Form.Label>
                        <Form.Control
                            onChange={this.handleChange}
                            type="text"
                            placeholder=""
                            value={address.streetB}
                        />
                    </Form.Group>
                    <Form.Group controlId="streetC">
                        <Form.Label>Street C</Form.Label>
                        <Form.Control
                            onChange={this.handleChange}
                            type="text"
                            placeholder=""
                            value={address.streetC}
                        />
                    </Form.Group>
                    <Form.Group controlId="streetD">
                        <Form.Label>Street D</Form.Label>
                        <Form.Control
                            onChange={this.handleChange}
                            type="text"
                            placeholder=""
                            value={address.streetD}
                        />
                    </Form.Group>
                    <hr/>
                    <Form.Group controlId="company.name">
                        <Form.Label>Company name</Form.Label>
                        <Form.Control
                            onChange={this.handleChange}
                            type="text"
                            placeholder=""
                            value={company.name}
                        />
                    </Form.Group>
                    <Form.Group controlId="company.catchPhrase">
                        <Form.Label>Catch phrase</Form.Label>
                        <Form.Control
                            onChange={this.handleChange}
                            type="text"
                            placeholder=""
                            value={company.streetD}
                        />
                    </Form.Group>
                    <Form.Group controlId="company.bs">
                        <Form.Label>BS(what is it???)</Form.Label>
                        <Form.Control
                            onChange={this.handleChange}
                            type="text"
                            placeholder=""
                            value={company.bs}
                        />
                    </Form.Group>
                </div>
            </Form>
        )
    }
}

export default ContactInfo;