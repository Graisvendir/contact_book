import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App'
import {CONTACT_LIST, getRequest, URL} from "./constants";

getRequest(URL).then((requestedData) => {
    localStorage.setItem(CONTACT_LIST, JSON.stringify(requestedData));
    ReactDOM.render(<App contactList={requestedData} />, document.getElementById("root"));
});
