# Contact book
### Setup and run
```sh
cd contact_book
npm i
npm run dev // for starting dev server
npm run build // for build app 
```
### Functional:
1. view contact list
2. view details of contact(history and posts on different tabs)
3. instant edit of contact info
4. search by names
### Features:
1. fix app view for desctop and tablet
2. adapt app for mobile
